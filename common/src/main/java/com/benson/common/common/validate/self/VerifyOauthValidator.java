package com.benson.common.common.validate.self;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 接口重写
 *
 * @author zhangby
 * @date 20/10/20 5:44 pm
 */
public class VerifyOauthValidator implements ConstraintValidator<VerifyOauth, Object> {
    private String role = "";

    @Override
    public void initialize(VerifyOauth constraintAnnotation) {
        role = constraintAnnotation.role();
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        if (ObjectUtil.isNull(o)) {
            return true;
        }
        if (StrUtil.isNotBlank(role) && o.toString().equals(role)) {
            return true;
        }
        return false;
    }
}
