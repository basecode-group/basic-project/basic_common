package com.benson.common.common.annotation;

import com.benson.common.common.CommonApplication;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Common 创建服务
 *
 * @author zhangby
 * @date 18/2/20 9:39 am
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({CommonApplication.class})
public @interface EnableCommonServer {
}
