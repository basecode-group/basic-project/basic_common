package com.benson.common.cron.entity.enums;

import com.benson.common.common.enums.BaseEnum;

/**
 * 定时任务状态
 *
 * @author zhangby
 * @date 11/12/19 5:42 pm
 */
public enum TriggerStateEnum implements BaseEnum {
    /**
     * NONE
     */
    NONE("无", "NONE"),
    /**
     * NORMAL
     */
    NORMAL("正常状态", "NORMAL"),
    /**
     * PAUSED
     */
    PAUSED("暂停状态", "PAUSED"),
    /**
     * COMPLETE
     */
    COMPLETE("完成", "COMPLETE"),
    /**
     * ERROR
     */
    ERROR("错误", "ERROR"),
    /**
     * BLOCKED
     */
    BLOCKED("堵塞", "BLOCKED");

    private String label;
    private String value;

    TriggerStateEnum(String label, String value) {
        this.label = label;
        this.value = value;
    }

    @Override
    public String getLabel() {
        return this.label;
    }

    @Override
    public String getValue() {
        return this.value;
    }
}