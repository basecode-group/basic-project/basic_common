package com.benson.common.cron.dto;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.benson.common.common.entity.BaseDto;
import com.benson.common.common.enums.StatusEnum;
import com.benson.common.common.util.CommonUtil;
import com.benson.common.common.util.EnumUtil;
import com.benson.common.cron.entity.Cron;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 定时任务 查询Dto
 *
 * @author zhangby
 * @date 28/2/20 5:01 pm
 */
@Data
@Accessors(chain = true)
public class CronQueryDto extends BaseDto<Cron> {
    private String keyword;
    private String status;

    @Override
    public LambdaQueryWrapper<Cron> queryWrapper() {
        LambdaQueryWrapper<Cron> queryWrapper = new LambdaQueryWrapper<Cron>()
                .orderByDesc(Cron::getCreateDate);
        // 查询条件设置
        CommonUtil.notEmpty(keyword)
                .ifPresent(key ->
                        queryWrapper.and(andQw ->
                                andQw.or(orQw -> orQw.like(Cron::getName, key))
                                        .or(orQw -> orQw.like(Cron::getMark, key))
                                        .or(orQw -> orQw.like(Cron::getCron, key))
                                        .or(orQw -> orQw.like(Cron::getRemarks, key))
                        )
                );
        // 查询状态
        CommonUtil.notEmpty(status)
                .map(item -> EnumUtil.initEnum(StatusEnum.class, item))
                .ifPresent(item ->
                        queryWrapper.eq(Cron::getStatus, item)
                );
        return queryWrapper;
    }
}
