package com.benson.common.cron.util;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.benson.common.common.util.SpringContextUtil;
import com.benson.common.cron.annotation.CronConfig;
import com.benson.common.cron.entity.CronCommonConfig;
import com.benson.common.cron.entity.CronInitEntity;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 原始定时任务缓存
 *
 * @author zhangby
 * @date 9/10/20 11:57 am
 */
public class CacheInitCronData {

    /**
     * 获取扫描包
     */
    private static final CronCommonConfig CRON_COMMON_CONFIG = SpringContextUtil.getBean(CronCommonConfig.class);

    /**
     * 初始数据
     */
    private static final Map<String, CronInitEntity> cronInitMap = MapUtil.newConcurrentHashMap();

    private static void init() {
        Set<Class<?>> classes = ClassUtil.scanPackage(CRON_COMMON_CONFIG.getScanPackage());
        List<Class<?>> classList = classes.stream()
                .filter(clazz -> ObjectUtil.isNotNull(clazz.getAnnotation(CronConfig.class)))
                .collect(Collectors.toList());
        classList.forEach(clazz -> {
            CronConfig cronConfig = clazz.getAnnotation(CronConfig.class);
            cronInitMap.put(cronConfig.value(), new CronInitEntity()
                    .setCron(cronConfig.cron())
                    .setValue(cronConfig.value())
                    .setDescription(cronConfig.description())
            );
        });
    }

    /**
     * 根据标识获取原始数据缓存
     */
    public static CronInitEntity getCronInit(String value) {
        if (StrUtil.isBlank(value)) {
            return null;
        }
        if (cronInitMap.isEmpty()) {
            init();
        }
        return cronInitMap.get(value);
    }
}
