package com.benson.common.system.annotation;

import com.benson.common.system.SystemApplication;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 系统功能服务
 *
 * @author zhangby
 * @date 17/2/20 6:35 pm
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@Import({SystemApplication.class})
public @interface EnableSystemService {
}
