package com.benson.common.creator;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@SpringBootApplication
@MapperScan("com.benson.common.creator.mapper")
public class CreatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(CreatorApplication.class, args);
    }

    @Value("${basic-common.creator.show:true}")
    private String show;

    @RequestMapping("/code/generator")
    public String index() {
        if ("false".equals(show)) {
            return null;
        }
        return "creator/creator-ui";
    }
}
