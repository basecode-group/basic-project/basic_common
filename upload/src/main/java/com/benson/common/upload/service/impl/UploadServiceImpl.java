package com.benson.common.upload.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.PageUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.benson.common.common.entity.Dict;
import com.benson.common.common.enums.DictTypeEnum;
import com.benson.common.common.util.CommonUtil;
import com.benson.common.common.util.DictUtil;
import com.benson.common.upload.dto.UploadConfigDto;
import com.benson.common.upload.entity.UploadItem;
import com.benson.common.upload.service.IUploadService;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 文件上传服务
 *
 * @author zhangby
 * @date 20/10/20 1:45 pm
 */
@Service
public class UploadServiceImpl implements IUploadService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public String getUploadFileSrc() {
        String fileSrc = DictUtil.getDictValue(DictTypeEnum.UPLOAD_CONFIG, "upload_file_src");
        String filePath = CommonUtil.notEmpty(fileSrc).orElse("classpath:/static/upload");
        if (filePath.contains("classpath:")) {
            try {
                filePath = filePath.replace("classpath:/", ResourceUtils.getURL("classpath:").getPath());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return filePath;
    }

    @Override
    public List<cn.hutool.core.lang.Dict> getFilePathTree(String path) {
        String fileSrc = getUploadFileSrc();
        try {
            return Stream.of(FileUtil.ls(fileSrc + Optional.ofNullable(path).orElse("")))
                    .filter(item -> item.isDirectory())
                    .sorted(Comparator.comparing(File::getName).reversed())
                    .map(item -> cn.hutool.core.lang.Dict.create()
                            .set("title", item.getName())
                            .set("key", item.getPath().replace(fileSrc, ""))
                    )
                    .collect(Collectors.toList());
        } catch (Exception e) {

        }
        return Lists.newArrayList();
    }

    @Override
    public List<cn.hutool.core.lang.Dict> getFilePathTreeInit(String path) {
        return getPath(getFilePathTree(path));
    }

    /**
     * 分页查询文件
     */
    @Override
    public cn.hutool.core.lang.Dict getFileList4Page(Page page, String path, String keyword) {
        String uploadFileSrc = getUploadFileSrc();
        List<File> files = FileUtil.loopFiles(uploadFileSrc + Optional.ofNullable(path).orElse(""),
                file -> {
                    boolean flag = !file.isHidden();
                    if (flag && StrUtil.isNotBlank(keyword)) {
                        flag = file.getName().contains(keyword);
                    }
                    return flag;
                }).stream()
                .sorted(Comparator.comparing(File::getPath).reversed())
                .collect(Collectors.toList());
        int total = files.size();
        List<cn.hutool.core.lang.Dict> records = Lists.newArrayList();
        int pageNo = (int) page.getCurrent() - 1;
        int pageSize = (int) page.getSize();
        if (total > 0) {
            // 参数判断
            if (pageNo < 0) {
                pageNo = 0;
            }
            int[] pageSizeArr = PageUtil.transToStartEnd(pageNo, pageSize);
            Integer first = pageSizeArr[0];
            Integer second = pageSizeArr[1] > total ? total : pageSizeArr[1];
            // 计算分页
            int totalPage = PageUtil.totalPage(total, pageSize);
            if (totalPage < pageNo) {
                pageNo = totalPage;
            }
            // 数据记录

            records = CollectionUtil.sub(files, first, second).stream()
                    .map(item -> cn.hutool.core.lang.Dict.create()
                            .set("name", item.getName())
                            .set("path", item.getPath().replace(uploadFileSrc, ""))
                    )
                    .collect(Collectors.toList());
        }
        // 返回数据
        return cn.hutool.core.lang.Dict.create()
                .set("total", total)
                .set("size", pageSize)
                .set("current", pageNo)
                .set("records", records);
    }

    @Override
    public UploadItem uploadVideo(MultipartFile file) {
        try {
            if (file.isEmpty()) {
                return UploadItem.error();
            }
            /** 判断视频类型 */
            String contentType = file.getContentType();
            if (!contentType.contains("video")) {
                return UploadItem.error();
            }
            String filePath = getUploadFileSrc() + "/video" + DateUtil.format(new Date(), "/yyyy/MM/");
            //替换项目路径
            //判断文件夹是否为空
            File dest = new File(filePath);
            if (dest.exists()) {
                FileUtil.mkdir(dest);
            }
            // 设置文件名
            String fileName = file.getOriginalFilename();
            String uuid = IdUtil.objectId();
            String imageName = uuid + fileName.substring(fileName.lastIndexOf("."), fileName.length());
            String imagePath = filePath + imageName;
            //上传文件
            FileUtil.writeBytes(file.getBytes(), imagePath);
            /** 获取返回地址 */
            String imgUrl = imagePath.replace(getUploadFileSrc(), "");
            return UploadItem.ok()
                    .setUid(uuid)
                    .setName(imageName)
                    .setUrl(imgUrl)
                    .setThumbUrl(imgUrl);
        } catch (IOException e) {
            logger.error(e.toString(), e);
        }
        return UploadItem.error();
    }

    // 上传其他文件
    @Override
    public UploadItem uploadOtherFile(MultipartFile file) {
        try {
            if (file.isEmpty()) {
                return UploadItem.error();
            }
            String filePath = getUploadFileSrc() + "/other" + DateUtil.format(new Date(), "/yyyy/MM/");
            //替换项目路径
            //判断文件夹是否为空
            File dest = new File(filePath);
            if (dest.exists()) {
                FileUtil.mkdir(dest);
            }
            // 设置文件名
            String fileName = file.getOriginalFilename();
            String uuid = IdUtil.objectId();
            String imageName = uuid + fileName.substring(fileName.lastIndexOf("."), fileName.length());
            String imagePath = filePath + imageName;
            //上传文件
            FileUtil.writeBytes(file.getBytes(), imagePath);
            /** 获取返回地址 */
            String imgUrl = imagePath.replace(getUploadFileSrc(), "");
            return UploadItem.ok()
                    .setUid(uuid)
                    .setName(imageName)
                    .setUrl(imgUrl)
                    .setThumbUrl(imgUrl);
        } catch (IOException e) {
            logger.error(e.toString(), e);
        }
        return UploadItem.error();
    }

    /**
     * 上传图片
     *
     * @param file
     * @return
     */
    @Override
    public UploadItem uploadImage(MultipartFile file) {
        try {
            if (file.isEmpty()) {
                return UploadItem.error();
            }
            /** 判断图片类型 */
            String contentType = file.getContentType();
            if (!contentType.contains("image")) {
                return UploadItem.error();
            }
            String filePath = getUploadFileSrc() + "/image" + DateUtil.format(new Date(), "/yyyy/MM/");
            //替换项目路径
            //判断文件夹是否为空
            File dest = new File(filePath);
            if (dest.exists()) {
                FileUtil.mkdir(dest);
            }
            // 设置文件名
            String fileName = file.getOriginalFilename();
            String uuid = IdUtil.objectId();
            String imageName = uuid + fileName.substring(fileName.lastIndexOf("."), fileName.length());
            String imagePath = filePath + imageName;
            //上传文件
            FileUtil.writeBytes(file.getBytes(), imagePath);
            /** 获取返回地址 */
            String imgUrl = imagePath.replace(getUploadFileSrc(), "");
            return UploadItem.ok()
                    .setUid(uuid)
                    .setName(imageName)
                    .setUrl(imgUrl)
                    .setThumbUrl(imgUrl);
        } catch (IOException e) {
            logger.error(e.toString(), e);
        }
        return UploadItem.error();
    }

    /**
     * 上传配置
     */
    @Override
    public void uploadConfigUpdate(UploadConfigDto uploadConfig) {
        List<Dict> dictList = DictUtil.getDictList4Type(DictTypeEnum.UPLOAD_CONFIG);
        // 循环更新
        dictList.forEach(item -> {
            String label = item.getLabel();
            String value = item.getValue();
            if ("upload_visit_url".equals(label) && !value.equals(uploadConfig.getVisitUrl())) {
                item.setValue(uploadConfig.getVisitUrl()).updateById();
            } else if ("upload_img_url".equals(label) && !value.equals(uploadConfig.getUploadImgUrl())) {
                item.setValue(uploadConfig.getUploadImgUrl()).updateById();
            } else if ("upload_file_src".equals(label) && !value.equals(uploadConfig.getFileSrc())) {
                item.setValue(uploadConfig.getFileSrc()).updateById();
            } else if ("upload_video_url".equals(label) && !value.equals(uploadConfig.getUploadVideoUrl())) {
                item.setValue(uploadConfig.getUploadVideoUrl()).updateById();
            } else if ("upload_other_url".equals(label) && !value.equals(uploadConfig.getUploadOtherUrl())) {
                item.setValue(uploadConfig.getUploadOtherUrl()).updateById();
            }
        });
        DictUtil.clear(DictTypeEnum.UPLOAD_CONFIG);
    }

    public List<cn.hutool.core.lang.Dict> getPath(List<cn.hutool.core.lang.Dict> parent) {
        if (parent.size() < 1) {
            return parent;
        }
        cn.hutool.core.lang.Dict dict = parent.get(0);
        List<cn.hutool.core.lang.Dict> pathTree = getFilePathTree(dict.getStr("key"));
        if (parent.size() > 0) {
            dict.set("children", getPath(pathTree));
        }
        return parent;
    }
}